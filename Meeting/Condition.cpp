#include <iostream>

#include "Condition.h"

int Condition::nextConditionNumber = 0;

Condition::Condition() : id(GenerateId())
{
	m1 = NULL;
	m2 = NULL;
	anotherObj = NULL;
	purpose = UNKNOWN;
}

Condition::Condition(MovingObject* m1, MovingObject* m2, Purpose p, MovingObject* another) : id(GenerateId())
{
	this->m1 = m1;
	this->m2 = m2;
	this->purpose = p;
	this->anotherObj = another;
}

Condition::~Condition()
{
	/* !Delete condition don't delete objects connected with it! */
}


//static method used to generate id for new condition
int Condition::GenerateId()
{
	return nextConditionNumber++;
}


//getters && setters
int Condition::getId()
{
	return id;
}

MovingObject* Condition::getObject1()
{
	return m1;
}
MovingObject* Condition::getObject2()
{
	return m2;
}

Purpose	Condition::getPurpose()
{
	return purpose;
}

char* Condition::getPurposeName()
{
	switch (purpose)
	{
	case CHANGE_DIRECTION_M1:
		return "CHANGE_DIRECTION_M1";
	case CHANGE_DIRECTION_M2:
		return "CHANGE_DIRECTION_M2";
	case CHANGE_DIRECTION_BOTH:
		return "CHANGE_DIRECTION_BOTH";
	case STOP_M1:
		return "STOP_M1";
	case STOP_M2:
		return "STOP_M2";
	case STOP_BOTH:
		return "STOP_BOTH";
	case STOP_ANOTHER_OBJECT:
		return "STOP_ANOTHER_OBJECT";
	case START_M1:
		return "START_M1";
	case START_M2:
		return "START_M2";
	case START_BOTH:
		return "START_BOTH";
	case START_ANOTHER_OBJECT:
		return "START_ANOTHER_OBJECT";
	case FINISH:
		return "FINISH";
	case UNKNOWN:
		return "UNKNOWN";
	default:
		return "";
	}
}

MovingObject* Condition::getAnotherObject()
{
	return anotherObj;
}

void Condition::setObject1(MovingObject* m)
{
	m1 = m;
}

void Condition::setObject2(MovingObject* m)
{
	m2 = m;
}

void Condition::setPurpose(Purpose p)
{
	purpose = p;
}

void Condition::setAnotherObject(MovingObject* m)
{
	anotherObj = m;
}

bool Condition::operator==(int id)
{
	if (this->id == id)
		return true;
	else
		return false;
}

bool Condition::operator==(Condition c)
{
	if (this->id == c.id)
		return true;
	else
		return false;
}
