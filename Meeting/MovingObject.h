#pragma once


/// A ------> B 

enum Direction { LEFT = 0, RIGHT };
enum ObjectState { STOPPED = 0, RUNNING };

class MovingObject
{
private:
	double speed;
	double time; //hours
	double distance; //place on the road
	double allDistance; //all moved distance 
	char name[32];
	Direction direction; //true = move right , false = move left 
	ObjectState state;

public:
	MovingObject();
	MovingObject(const char* name, 
		double speed, 
		double distance, 
		Direction d, 
		double starting_time = 0.0, 
		double allDist = 0.0, 
		ObjectState state = RUNNING);

	~MovingObject();

	void ChangeDirection();
	void Run();
	bool operator==(MovingObject m);	// compare two objects by name
	bool operator==(const char* name);

	double getTime();
	double getDistance();
	double getAllDistance();
	Direction getDirection();
	char* getName();
	double getSpeed();
	ObjectState getState();

	void setStartingTime(double hours);
	void setDistance(double dist);
	void setDirection(Direction d);
	void setName(char* name);
	void setSpeed(double speed);
	void setState(ObjectState state);
};
