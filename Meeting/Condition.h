#pragma once

#include "MovingObject.h"

//purpose of meeting 
enum Purpose { CHANGE_DIRECTION_M1 = 0, CHANGE_DIRECTION_M2, CHANGE_DIRECTION_BOTH,
	STOP_M1, STOP_M2, STOP_BOTH, STOP_ANOTHER_OBJECT,
	START_M1, START_M2, START_BOTH, START_ANOTHER_OBJECT,
	FINISH, UNKNOWN };


class Condition
{
	friend class Application;

private:
	const int id;
	MovingObject* m1;
	MovingObject* m2;
	Purpose purpose;
	
	MovingObject* anotherObj;

protected:
	static int nextConditionNumber;
	static int GenerateId();

public:
	Condition();
	Condition(MovingObject* m1, MovingObject* m2, Purpose p, MovingObject* another = NULL);
	~Condition();

	//getters && setters
	int getId();
	MovingObject* getObject1();
	MovingObject* getObject2();
	Purpose getPurpose();
	char* getPurposeName();
	MovingObject* getAnotherObject();
	
	void setObject1(MovingObject*);
	void setObject2(MovingObject*);
	void setPurpose(Purpose);
	void setAnotherObject(MovingObject*);

	bool operator==(int id);
	bool operator==(Condition c);

	
};
