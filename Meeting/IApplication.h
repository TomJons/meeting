#pragma once

#include <list>
#include "MovingObjectManager.h"

class IApplication
{
public:
	virtual void ShowList() = 0;
	virtual bool Add() = 0;
	virtual bool Remove() = 0; 
	virtual bool MainCheckCond() = 0;
	virtual bool AddSideCheckCond() = 0;
	virtual bool AddSideEffect() = 0;
	virtual bool ShowStatistics() = 0;
	virtual bool DetermineExactitude() = 0;
};


//will use console
class ConsoleApplication : public IApplication
{
private:
	std::list<MovingObject*> temp;	//tymczasowa lista
	MovingObject* m;			//obiekt 
	char* name;					//nazwa

	bool EnterName();			//metoda pozwalajaca userowi wpisac nazwe

protected:
	MovingObjectManager* mom;

public:
	ConsoleApplication();
	~ConsoleApplication();

	virtual void ShowList();
	virtual bool Add();
	virtual bool Remove();
	virtual bool MainCheckCond();
	virtual bool AddSideCheckCond();
	virtual bool AddSideEffect();
	virtual bool ShowStatistics();
	virtual bool DetermineExactitude();
	void StartMenu();

	
};


//will use wxWidgets
class GuiApplication : public IApplication
{
	virtual void ShowList();
	virtual bool Add();
	virtual bool Remove();
	virtual bool MainCheckCond();
	virtual bool AddSideCheckCond();
	virtual bool AddSideEffect();
	virtual bool ShowStatistics();
	virtual bool DetermineExactitude();
};
