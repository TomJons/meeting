#include <iostream>
#include <string.h>
#include <list>

#include "Application.h"

using namespace pugi;
using namespace std;

Application::~Application()
{
}

//removing all objects
void Application::RemoveAllObjects()
{
	MovingObjectManager* mom = MovingObjectManager::getInstance();
	mom->RemoveAllObjects();
}

//removing all conditions
void Application::RemoveAllConditions()
{
	MovingObjectManager* mom = MovingObjectManager::getInstance();
	mom->RemoveAllConditions();
}

//method - call the manager to remove all objects and conditions from list
void Application::RemoveAll()
{
	MovingObjectManager* mom = MovingObjectManager::getInstance();
	mom->RemoveAll();
}

//method that save to .xml file all objects currently exist in the list
//and also save meeting condition
bool Application::Save(char* filename)
{
	xml_document doc;
	xml_node meeting = doc.append_child("Meeting");
	meeting.append_child("Objects");
	meeting.append_child("Conditions");

	SaveObjects(doc);
	SaveConditions(doc);

	if (doc.save_file(filename))
		return true;
	else
		return false;
}

//method saves objects
void Application::SaveObjects(pugi::xml_document & doc)
{
	xml_node objects = doc.child("Meeting").child("Objects");
	MovingObjectManager* mom = MovingObjectManager::getInstance();
	list<MovingObject*> listOfLoadedObjects = mom->getObjectsList();

	for (list<MovingObject*>::iterator iter = listOfLoadedObjects.begin(); iter != listOfLoadedObjects.end(); iter++)
	{
		xml_node obj = objects.append_child("Object");
		obj.append_attribute("name") = (*iter)->getName();
		obj.append_attribute("speed") = (*iter)->getSpeed();
		obj.append_attribute("distance") = (*iter)->getDistance();
		obj.append_attribute("direction") = int((*iter)->getDirection());
		obj.append_attribute("time") = (*iter)->getTime();
		obj.append_attribute("allDistance") = (*iter)->getAllDistance();
		obj.append_attribute("state") = int((*iter)->getState());
	}
}

//method saves conditions
void Application::SaveConditions(pugi::xml_document & doc)
{
	xml_node conditions = doc.child("Meeting").child("Conditions");
	MovingObjectManager* mom = MovingObjectManager::getInstance();
	list<Condition*> listOfLoadedConditions = mom->getConditionsList();

	for (list<Condition*>::iterator iter = listOfLoadedConditions.begin(); iter != listOfLoadedConditions.end(); iter++)
	{
		xml_node cond = conditions.append_child("Condition");
		cond.append_attribute("m1") = (*iter)->getObject1()->getName();
		cond.append_attribute("m2") = (*iter)->getObject2()->getName();
		cond.append_attribute("purpose") = int((*iter)->getPurpose());
		cond.append_attribute("ma") =  ((*iter)->getAnotherObject() ? (*iter)->getAnotherObject()->getName() : "NULL");
	}
}

//method that load from .xml file all objects to the list
//and also load meeting condition
bool Application::Load(char* filename)
{
	xml_document doc;
	if (!doc.load_file(filename))
		return false;

	LoadObjects(doc);
	LoadConditions(doc);

	return true;
}

//method loads objects
void Application::LoadObjects(xml_document & doc)
{
	MovingObject* m;
	list<MovingObject*> listOfLoadedObjects;
	MovingObjectManager* mom = MovingObjectManager::getInstance();
	xml_node objects = doc.child("Meeting").child("Objects");

	for (xml_node obj = objects.first_child(); obj; obj = obj.next_sibling())
	{
		m = new MovingObject(
			obj.attribute("name").value(),
			obj.attribute("speed").as_double(),
			obj.attribute("distance").as_double(),
			Direction(obj.attribute("direction").as_int()),
			obj.attribute("time").as_double(),
			obj.attribute("allDistance").as_double(),
			ObjectState(obj.attribute("state").as_int())
			);
		listOfLoadedObjects.push_back(m);
	}
	mom->AddObjects(listOfLoadedObjects);
}

//method loads conditions
void Application::LoadConditions(xml_document & doc)
{
	Condition::nextConditionNumber = 0;

	Condition* c;
	list<Condition*> listOfLoadedConditions;
	MovingObjectManager* mom = MovingObjectManager::getInstance();
	xml_node conditions = doc.child("Meeting").child("Conditions");

	for (xml_node cond = conditions.first_child(); cond; cond = cond.next_sibling())
	{
		c = new Condition(
			mom->getObjectByName(cond.attribute("m1").value()),
			mom->getObjectByName(cond.attribute("m2").value()),
			Purpose(cond.attribute("purpose").as_int()),
			mom->getObjectByName(cond.attribute("ma").value())
			);
		listOfLoadedConditions.push_back(c);
	}
	mom->AddCondition(listOfLoadedConditions);
}
