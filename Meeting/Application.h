#pragma once

#include <list>
#include "MovingObjectManager.h"
#include "pugixml-1.4/src/pugixml.hpp"

class IApplication
{
public:
	virtual void ShowObjectsList() = 0;
	virtual void ShowConditionsList() = 0;
	virtual bool AddObject() = 0;
	virtual bool RemoveObject() = 0; 
	virtual void RemoveAllObjects() = 0;
	virtual bool AddCondition() = 0;
	virtual bool RemoveCondition() = 0;
	virtual void RemoveAllConditions() = 0;
	virtual void RemoveAll() = 0;
	virtual bool ShowStatistics() = 0;
	virtual bool DetermineExactitude() = 0;
	virtual bool Save(char* filename) = 0;
	virtual bool Load(char* filename) = 0;
	virtual bool StartMenu() = 0; 
};

//*****************************************************
//base class for console and gui 
class Application : public IApplication
{
private:
	void LoadObjects(pugi::xml_document & doc);
	void LoadConditions(pugi::xml_document & doc);
	void SaveObjects(pugi::xml_document & doc);
	void SaveConditions(pugi::xml_document & doc);

public:
	virtual void RemoveAllObjects();
	virtual void RemoveAllConditions();
	virtual void RemoveAll();
	virtual bool Save(char* filename);
	virtual bool Load(char* filename);
	virtual ~Application();
};

//*****************************************************
//will use console
class ConsoleApplication : public Application
{
private:			
	bool QuitAction();
	void CinError(bool & val, const char* text);
	int MenuTest(bool & val);
	void Underscore();
	template<typename T> bool EnterData(T & val, char* text);
	template<typename T> bool EnterData(T & val, char* text, char* wrongText);
	bool EnterData(char* val, char* text);
	bool EnterData(Direction & val, char* text);
	bool EnterData(ObjectState & val, char* text);
	bool EnterData(Purpose & val, char* text);
	void GenerateErrorMessage(char* text);
	void RemoveAllObjectsAction();
	bool CheckRemoveAll(char* text);
	MovingObject* EnterObject(char* text); 
	void RemoveAllConditionsAction();
	void RemoveAllAction();
	void LoadAction();
	void SaveAction();

protected:
	MovingObjectManager* mom;

public:
	ConsoleApplication();
	~ConsoleApplication();
	
	virtual bool StartMenu();
	virtual void ShowObjectsList();
	virtual void ShowConditionsList();
	virtual bool AddObject();
	virtual bool RemoveObject();
	virtual bool AddCondition();
	virtual bool RemoveCondition();
	virtual bool ShowStatistics();
	virtual bool DetermineExactitude();


};

//*****************************************************
//will use wxWidgets
class GuiApplication : public Application
{
public:
	virtual bool StartMenu();
	virtual void ShowObjectsList();
	virtual void ShowConditionsList();
	virtual bool AddObject();
	virtual bool RemoveObject();
	virtual bool AddCondition();
	virtual bool RemoveCondition();
	virtual bool ShowStatistics();
	virtual bool DetermineExactitude();
};
