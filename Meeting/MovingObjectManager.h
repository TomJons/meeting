#pragma once

#include <list>

#include "MovingObject.h"
#include "Condition.h"

class MovingObjectManager
{
private:
	static MovingObjectManager* _instance;
	std::list<MovingObject*> listOfObjects;
	std::list<Condition*> listOfConditions;

protected:
	MovingObjectManager();

public:
	static MovingObjectManager* getInstance();

	~MovingObjectManager();

	void AddObjects(MovingObject* m);
	void AddObjects(std::list<MovingObject*> lm);
	void RemoveObj(MovingObject* m);
	void RemoveAllObjects();
	void AddCondition(Condition* c); 
	void AddCondition(std::list<Condition*> lc); 
	void RemoveCondition(Condition* c); 
	void RemoveAllConditions();
	void RemoveAll();
	std::list<MovingObject*> & getObjectsList();
	std::list<Condition*> & getConditionsList();
	MovingObject* getObjectByName(const char* name);
	Condition* getConditionById(int id);
};
