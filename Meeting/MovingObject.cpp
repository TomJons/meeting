#include <iostream>
#include "MovingObject.h"
#include <string.h>
using namespace std;

MovingObject::MovingObject()
{
	allDistance = 0.0;
}


MovingObject::~MovingObject()
{
}

MovingObject::MovingObject(const char* name, double speed, double distance, Direction d, double starting_time, double allDist, ObjectState state)
{
	this->speed = speed;
	this->time = starting_time;
	this->direction = d;
	this->distance = distance;
	this->allDistance = allDist;
	strcpy_s(this->name, name);
	this->state = state;
}


void MovingObject::ChangeDirection()
{
	if (direction == RIGHT)
		direction = LEFT;
	else
		direction = RIGHT;
}

void MovingObject::Run()
{
	//t = s/v
	//v = s/t
	//s = tv
	
	double h = 1.0 / 360000;
	time += h; // 1/100 sekundy
	double d = h * speed;

	if (direction == RIGHT)
		distance += d;
	else
		distance -= d;

 	allDistance += d;
}

double MovingObject::getTime()
{
	return time;
}

double MovingObject::getDistance()
{
	return distance;
}

Direction MovingObject::getDirection()
{
	return direction;
}

double MovingObject::getAllDistance()
{
	return allDistance;
}

char* MovingObject::getName()
{
	return name;
}

double MovingObject::getSpeed()
{
	return speed;
}

//settery
void MovingObject::setStartingTime(double hours)
{
	this->time = hours;
}

void MovingObject::setDistance(double dist)
{
	this->distance = dist; 
}

void MovingObject::setDirection(Direction d)
{
	this->direction = d;
}

void MovingObject::setName(char* name)
{
	strcpy_s(this->name, name);
}

void MovingObject::setSpeed(double speed)
{
	this->speed = speed;
}

// compare two objects by name
bool MovingObject::operator==(MovingObject m)
{
	if (!strcmp(this->name, m.name))
		return true;
	else
		return false;
}

bool MovingObject::operator==(const char* name)
{
	if (!strcmp(this->name, name))
		return true;
	else
		return false;
}


ObjectState MovingObject::getState()
{
	return state;
}


void MovingObject::setState(ObjectState state)
{
	this->state = state;
}
