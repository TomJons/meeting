#include <iostream>
#include <iomanip>
#include <list>
#include <cstdlib>
#include "Application.h"

using namespace std;

//constr
ConsoleApplication::ConsoleApplication()
{
	mom = MovingObjectManager::getInstance();
}

//dest
ConsoleApplication::~ConsoleApplication()
{
	delete mom;
	mom = NULL;
}

//method responsible for show all avaliable options and lunch approprite
bool ConsoleApplication::StartMenu()
{
	int menu;
	bool goodChoice = false;
	do
	{
		cout << "> Welcome in the Meeting Application!What you want to do ?" << endl;
		menu = MenuTest(goodChoice);
	} while (!goodChoice);
		
	switch (menu)
	{
	case 1:
		ShowObjectsList();
		break;

	case 2:
		ShowConditionsList();
		break;

	case 3:
		AddObject();
		break;

	case 4:
		RemoveObject();
		break;

	case 5:
		RemoveAllObjectsAction();
		break;

	case 6:
		AddCondition(); 
		break;

	case 7:
		RemoveCondition();
		break;

	case 8:
		RemoveAllConditionsAction();
		break;

	case 9:
		RemoveAllAction();
		break;

	case 10:
		ShowStatistics();
		break;

	case 11:
		DetermineExactitude();
		break;

	case 12:
		SaveAction();
		break;

	case 13:
		LoadAction();
		break;

	case 99:
		if (QuitAction()) 
			return false;
		break;

	default:

		break;
	}
	return true;
}

//method shows objects as list
void ConsoleApplication::ShowObjectsList()
{
	list<MovingObject*> temp_o = mom->getObjectsList();
	int counter = 0;

	cout << setw(2) << left << "Nr" << ": "
		<< setw(32) << left << "Name"
		<< setw(9) << left << "Time"
		<< setw(8) << left << "Speed"
		<< setw(9) << left << "OnRoad"			//distance
		<< setw(8) << left << "AllDist"			//all
		<< setw(6) << left << "Move"			//direction 
		<< setw(4) << left << "Run"				//running or stopped
		<< endl;

	for (list<MovingObject*>::iterator iter = temp_o.begin(); iter != temp_o.end(); iter++)
	{
		cout << setw(2) << left << counter++ << ": "
			<< setw(32) << left << (*iter)->getName()
			<< setw(9) << left << (*iter)->getTime()
			<< setw(8) << left << (*iter)->getSpeed()
			<< setw(9) << left << (*iter)->getDistance()
			<< setw(8) << left << (*iter)->getAllDistance()
			<< setw(6) << left << ((*iter)->getDirection() == RIGHT ? "RIGHT" : "LEFT")
			<< setw(4) << left << ((*iter)->getState() == RUNNING ? "R" : "S")
			<< endl;
	}

	//cosmetic manipulation
	Underscore();
}

//method shows condition list
void ConsoleApplication::ShowConditionsList()
{
	list<Condition*> temp_c = mom->getConditionsList();
	
	cout << setw(2) << left << "ID" << ": "
		<< setw(20) << left << "Object1"
		<< setw(20) << left << "Object2"
		<< setw(21) << left << "Purpose"
		<< setw(15) << left << "Another Object"
		<< endl;

	for (list<Condition*>::iterator iter = temp_c.begin(); iter != temp_c.end(); iter++)
	{
		cout << setw(2) << left << (*iter)->getId() << ": "
			<< setw(20) << left << (*iter)->getObject1()->getName()
			<< setw(20) << left << (*iter)->getObject2()->getName()
			<< setw(21) << left << (*iter)->getPurposeName()
			<< setw(15) << left << ((*iter)->getAnotherObject() ? (*iter)->getAnotherObject()->getName() : "")    
			<< endl;
	}
	
	Underscore();
}

//method adds new object
bool ConsoleApplication::AddObject()	//poprawic obsluge bledow!
{	
	double time, speed, dist;
	Direction dir;
	ObjectState state;
	char name[32];

	if (!EnterData(name, "name of object"))
		return false;

	if (!EnterData(time, "starting time[1hour = 1.0]"))
		return false;
	
	if (!EnterData(speed, "speed [1km/h = 1.0]"))
		return false;

	if (!EnterData(dist, "starting position on the road [ex. 0.0]"))
		return false;

	if (!EnterData(dir, "direction that object will move [L = LEFT; R = RIGHT]"))
		return false;

	if (!EnterData(state, "object state [R = RUNNING; S = STOPPED]"))
		return false;
	
	MovingObject* m = new MovingObject(name, speed, dist, dir, time, 0.0, state);
	mom->AddObjects(m);
	
	cout << endl << "Object was successfully added!" << endl;

	Underscore();

	return true;
}

//method deletes object by name 
bool ConsoleApplication::RemoveObject()
{
	char name[32];
	if (!EnterData(name, "name of object you want to remove"))
		return false;

	MovingObject* m = mom->getObjectByName(name);

	//object not found
	if (!m)
	{
		cout << endl << "> Object not found!" << endl << endl;
		Underscore();
		return false;
	}

	//free m memory and set m to NULL in that function
	mom->RemoveObj(m);

	cout << endl << "> Object was successfuly removed" << endl << endl;
	Underscore();

	return true;
}

//method adds a condition
bool ConsoleApplication::AddCondition()
{
	MovingObject* m1 = NULL;
	MovingObject* m2 = NULL;
	Purpose p;
	MovingObject* another = NULL;
	
	cout << "> Object1 is going to meet object2, enter their names and purpose of meeting" << endl;
	if (!(m1 = EnterObject("name of object1")))
		return false;

	if (!(m2 = EnterObject("name of object2")))
		return false;

	if (*m1 == *m2)
	{
		cout << "> Object can't meet with itself.. Wrong condition" << endl << endl;
		Underscore();
		return false;
	}

	char* purposeText = "purpose of meeting\n" 
		 "0 - Change Direction Object1\n"
		 "1 - Change Direction Object2\n"
		 "2 - Change Direction Both Objects\n"
		 "3 - Stop Object1\n"
		 "4 - Stop Object2\n"
		 "5 - Stop Both Objects\n"
		 "6 - Stop Another Object\n"
		 "7 - Start Object1\n"
		 "8 - Start Object2\n"
		 "9 - Start Both Objects\n"
		 "10 - Start Another Object\n"
		 "11 - Finish\n"
		 "12 - Unknown\n";

	if (!EnterData(p, purposeText))
		return false;

	switch (p)
	{
	case STOP_ANOTHER_OBJECT:
	case START_ANOTHER_OBJECT:
		cin.ignore(1000, '\n');
		if (!(another = EnterObject("name of another object")))
			return false;
		break;

	default:
		break;
	}

	Condition* c = new Condition(m1, m2, p, another);
	mom->AddCondition(c);

	cout << endl << "Condition was successfully added!" << endl;
	Underscore();

	return true;
}

//method deletes a condition
bool ConsoleApplication::RemoveCondition()
{
	int id;
	if (!EnterData(id, "id of condition you want to remove"))
		return false;

	Condition* c = mom->getConditionById(id);

	//condition not found
	if (!c)
	{
		cout << endl << "> Condition not found!" << endl << endl;
		Underscore();
		return false;
	}

	//free m memory and set m to NULL in that function
	mom->RemoveCondition(c);

	cout << endl << "> Condition was successfuly removed" << endl << endl;
	Underscore();

	return true;;
}

bool ConsoleApplication::ShowStatistics()
{
	return true;
}
bool ConsoleApplication::DetermineExactitude()
{
	return true;
}

//method is responsible for quit and asking to save or not  
bool ConsoleApplication::QuitAction()
{
	int choose;
	cout << "> Are you sure to quit? Press Q to quit without changes; Press S to quit within changes; Press C to Cancel" << endl;
	choose = cin.get();
	
	switch (choose)
	{
	case 'Q':
	case 'q':
		cin.ignore(1000, '\n');
		return true;

	case 'S':
	case 's':
		cin.ignore(1000, '\n');
		SaveAction();
		return true;

	case 'C':
	case 'c':
		cin.ignore(1000, '\n');
		return false;

	default:
		GenerateErrorMessage("confirmation");
		return false;
	}
}

//method provides support for wrong user choose
void ConsoleApplication::CinError(bool & val, const char* text)
{
	cin.clear();
	cin.ignore(1000, '\n');
	cerr << "\n> " << text << '\n' << endl;
	val = false;
	Underscore();
}

//method provides and test user choose in menu 
int ConsoleApplication::MenuTest(bool & val)
{
	int menu = -1;

	char* menuText = "\n"
		"1 - Show Objects List\n"
		"2 - Show Conditions List\n"
		"3 - Add Object\n"
		"4 - Remove Object\n"
		"5 - Remove All Objects\n"
		"6 - Add Condition\n"
		"7 - Remove Condition\n"
		"8 - Remove All Conditions\n"
		"9 - Remove All Objects And Conditions\n"
		"10 - Show Statistics\n"
		"11 - Determine Exactitude\n"
		"12 - Save data to .xml\n"
		"13 - Load data from .xml\n"
		"99 - Quit the program\n";

	if (!EnterData(menu, menuText, "Wrong choose"))
	{
		val = false;
		menu = -1;
	}
	else if (menu < 1 || (menu > 13 && menu != 99))
		CinError(val, "Wrong choose! Choose again!");
	else
	{
		val = true;
		cin.ignore(1000, '\n');
	}

	return menu;
}

//cosmetic manipulation of output 
void ConsoleApplication::Underscore()
{
	for (int i = 0; i < 80; i++)
	{
		cout << "=";
	}
	cin.ignore(1000, '\n');
	cout << endl;
}

//template method responsible for enter data by user speed, distance, etc.
template<typename T> bool ConsoleApplication::EnterData(T & val , char* text)
{
	cout << "> Enter: " << text << endl;
	if (!(cin >> val))
	{
		GenerateErrorMessage(text);
		return false;
	}

	return true;
}

//method with presentation text and different error text
template<typename T> bool ConsoleApplication::EnterData(T & val, char* text, char* wrongText)
{
	cout << "> Enter: " << text << endl;
	if (!(cin >> val))
	{
		GenerateErrorMessage(wrongText);
		return false;
	}

	return true;
}


//specialization for name 
 bool ConsoleApplication::EnterData(char* name, char* text)
{
	cout << "> Enter: " << text << endl;
	if (!cin.getline(name, 32, '\n'))
	{
		GenerateErrorMessage(text);
		return false;
	}
	
	return true;
}

//specialization for direction
bool ConsoleApplication::EnterData(Direction & val, char* text)
{
	int cdir;
	cin.ignore(1000, '\n');
	cout << "> Enter: " << text << endl;
	cdir = cin.get();

	if (cdir == 'L')
		val = LEFT;
	else if (cdir == 'R')
		val = RIGHT;
	else
	{
		GenerateErrorMessage(text);
		return false;
	}

	return true;

}

//specialization for state
bool ConsoleApplication::EnterData(ObjectState & val, char* text)
{
	int cdir;
	cin.ignore(1000, '\n');
	cout << "> Enter: " << text << endl;
	cdir = cin.get();

	if (cdir == 'R')
		val = RUNNING;
	else if (cdir == 'S')
		val = STOPPED;
	else
	{
		GenerateErrorMessage(text);
		return false;
	}

	cin.ignore(1000, '\n');
	return true;
}

//specialization for purpose
bool ConsoleApplication::EnterData(Purpose & val, char* text)
{
	int value;

	if (!EnterData(value, text, "Wrong choose"))
		return false;

	if (value < 0 || value > 12)
	{
		cout << "\n> Wrong choose!" << endl << endl;
		Underscore();
		return false;
	}

	val = (Purpose)value;
	return true;
}

//method makes error message
void ConsoleApplication::GenerateErrorMessage(char* text)
{
	string errorMessage = "Wrong : ";
	errorMessage += text;
	errorMessage += "!";
	bool nothing;
	CinError(nothing, errorMessage.c_str());
}

//method calls the manager to remove all objects and display communicate
void ConsoleApplication::RemoveAllObjectsAction()
{
	if (CheckRemoveAll("objects"))
	{
		RemoveAllObjects();
		cout << endl << "> All Objects were removed" << endl;
		Underscore();
	}
	else
	{
		cout << endl << "> Operation canceled" << endl;
		Underscore();
	}
}

//method asks the question to remove all 
bool ConsoleApplication::CheckRemoveAll(char* text)
{
	int choose;
	cout << "Are you sure to remove all " << text << "? [Y/N]" << endl;
	choose = cin.get();

	switch (choose)
	{
	case 'Y':
	case 'y':
		cin.ignore(1000, '\n');
		return true;

	case 'N':
	case 'n':
		cin.ignore(1000, '\n');
		return false;

	default:
		GenerateErrorMessage("confirmation");
		return false;
	}
}

//method using for connect object with condition
MovingObject* ConsoleApplication::EnterObject(char* text)
{
	MovingObject* m = NULL;
	char name[32];

	if (!EnterData(name, text))
		return NULL;

	m = mom->getObjectByName(name);

	//object not found
	if (!m)
	{
		cout << endl << "> Object not found!" << endl << endl;
		Underscore();
		return NULL;
	}

	return m;
}

//method calls the manager to remove all conditions and display communicate
void ConsoleApplication::RemoveAllConditionsAction()
{
	if (CheckRemoveAll("conditions"))
	{
		RemoveAllConditions();
		cout << endl << "> All conditions were removed" << endl;
		Underscore();
	}
	else
	{
		cout << endl << "> Operation canceled" << endl;
		Underscore();
	}
}

//method calls the manager to remove all objects and conditions and display communicate
void ConsoleApplication::RemoveAllAction()
{
	if (CheckRemoveAll("objects and conditions"))
	{
		RemoveAll();
		cout << endl << "> All objects and conditions were removed" << endl;
		Underscore();
	}
	else
	{
		cout << endl << "> Operation canceled" << endl;
		Underscore();
	}
}

//method provides action for load from .xml
void ConsoleApplication::LoadAction()
{
	if (CheckRemoveAll("objects and conditions to load data from .xml file"))
	{
		char filename[32];
		if (!EnterData(filename, "name of file within .xml extension"))
			return;

		RemoveAll();
		if (Load(filename))
		{
			cout << endl << "> Objects and conditions were successfuly loaded" << endl;
			Underscore(); 
		}
		else
		{
			cout << endl << "> Loading operation failed.." << endl;
			Underscore();
		}
	}
	else
	{
		cout << endl << "> Operation canceled" << endl;
		Underscore();
	}
}

//method provides save action
void ConsoleApplication::SaveAction()
{
	char filename[32];
	if (!EnterData(filename, "name of file within .xml extension"))
		return;

	if (Save(filename))
	{
		cout << endl << "> Objects and conditions were successfuly saved" << endl;
		Underscore();
	}
	else
	{
		cout << endl << "> Saving operation failed.." << endl;
		Underscore();
	}
}
