#include <iostream>
#include <conio.h>
#include <iomanip>

#include "Application.h"


using namespace std;

bool check(MovingObject* p1, MovingObject* p2)
{
	double wrong = 0.001; //1 metr

	if (fabs(p1->getDistance() - p2->getDistance()) < wrong)
		return true;
	else
		return false;
}


int main()
{
	
	MovingObjectManager* mom = MovingObjectManager::getInstance();
	
	//predefined objects
	MovingObject* programist = new MovingObject("Programista", 25, 0, RIGHT);
	MovingObject* tester = new MovingObject("Tester", 75, 300, LEFT);
	MovingObject* manager = new MovingObject("Project Manger", 100, 0, RIGHT);

	mom->AddObjects(programist);
	mom->AddObjects(tester);
	mom->AddObjects(manager);


	//wersja konsolowa
	IApplication* app = new ConsoleApplication();

	bool retval;
	do
	{
		retval = app->StartMenu();
	} while (retval);


	while (!check(programist, tester))
	{
		manager->Run();
		programist->Run();
		tester->Run();
		//until case 1 or 2 will be true
		while (!((manager->getDirection() == RIGHT && check(manager, tester)) 
			|| (manager->getDirection() == LEFT && check(manager, programist))))
		{
			manager->Run();
			programist->Run();
			tester->Run();
		}
		cout << "M: " << manager->getDistance() << " " << "P: " << programist->getDistance() << " " << "T: " << tester->getDistance() << endl;
		cout << "Manager changed direction" << endl;
		manager->ChangeDirection();
	}

	cout << manager->getDistance() << "dziekuje! <3" << endl;
	cout << setprecision(10) << manager->getAllDistance() << endl;


	delete mom;
	mom = NULL;
	delete app;
	app = NULL;
	_getch();
}
