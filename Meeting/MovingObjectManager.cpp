#include <list>
#include <iostream>

#include "MovingObjectManager.h"
using namespace std;

//declaration of static members
MovingObjectManager* MovingObjectManager::_instance = NULL;

//method that provide only instance of manager (singleton)
MovingObjectManager* MovingObjectManager::getInstance()
{
	if (!_instance)
		_instance = new MovingObjectManager();

	return _instance;
}

//constructor
MovingObjectManager::MovingObjectManager()
{
}

//destructor
MovingObjectManager::~MovingObjectManager()
{
	//todo : question about saving objects or not

	//deleting objects
	if (!listOfObjects.empty())
	{
		for (list<MovingObject*>::iterator iter = listOfObjects.begin(); iter != listOfObjects.end(); iter++)
		{
			delete *iter;
		}
	}

	//deleting conditions
	if (!listOfConditions.empty())
	{
		for (list<Condition*>::iterator iter = listOfConditions.begin(); iter != listOfConditions.end(); iter++)
		{
			delete *iter;
		}
	}
}


//methods adding objects
void MovingObjectManager::AddObjects(MovingObject* m)
{
	listOfObjects.push_back(m);
}

void MovingObjectManager::AddObjects(list<MovingObject*> lm)
{
	listOfObjects.splice(listOfObjects.end(), lm);
}

//Removing objects - free memory from pointer!
void MovingObjectManager::RemoveObj(MovingObject* m)
{
	//deleting objects from list
	listOfObjects.remove(m);
	//free memory
	delete m;
	m = NULL; 
}

void MovingObjectManager::RemoveAllObjects()
{
	//deleting objects
	if (!listOfObjects.empty())
	{
		for (list<MovingObject*>::iterator iter = listOfObjects.begin(); iter != listOfObjects.end(); iter++)
		{
			delete *iter;
		}
	}
	
	listOfObjects = list<MovingObject*>();

}

//methods concern Conditions
void MovingObjectManager::AddCondition(Condition* c)
{
	listOfConditions.push_back(c);
}

void MovingObjectManager::AddCondition(list<Condition*> lc)
{
	listOfConditions.splice(listOfConditions.end(), lc);
}

void MovingObjectManager::RemoveCondition(Condition* c)
{
	//deleting objects from list
	listOfConditions.remove(c);
	//free memory
	delete c;
	c = NULL;
}

void MovingObjectManager::RemoveAllConditions()
{
	if (!listOfConditions.empty())
	{
		for (list<Condition*>::iterator iter = listOfConditions.begin(); iter != listOfConditions.end(); iter++)
		{
			delete *iter;
		}
	}

	listOfConditions = list<Condition*>();
}

//removing all conditions and objects
void MovingObjectManager::RemoveAll()
{
	RemoveAllObjects();
	RemoveAllConditions();
}

//return list 
list<MovingObject*> & MovingObjectManager::getObjectsList()
{
	return listOfObjects;
}

list<Condition*> & MovingObjectManager::getConditionsList()
{
	return listOfConditions;
}

//get object by name, if find return pointer to an object otherwise return NULL
MovingObject* MovingObjectManager::getObjectByName(const char* name)
{
	MovingObject* m = NULL;

	if (listOfObjects.empty())
		return NULL;

	for (list<MovingObject*>::iterator iter = listOfObjects.begin(); iter != listOfObjects.end(); iter++)
	{
		if (*(*iter) == name)
		{
			m = *iter;
			break;
		}
	}

	return m;
}

//get condition by id 
Condition* MovingObjectManager::getConditionById(int id)
{
	Condition* c = NULL;

	if (listOfConditions.empty())
		return NULL;

	for (list<Condition*>::iterator iter = listOfConditions.begin(); iter != listOfConditions.end(); iter++)
	{
		if (*(*iter) == id)
		{
			c = *iter;
			break;
		}
	}

	return c;
}
